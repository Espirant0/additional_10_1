#define   Name       "Espiranto program"
; ������ ����������
#define   Version    "1.0"
; �����-�����������
#define   Publisher  "Espiranto"
; ��� ������������ ������
#define   ExeName    "sfml_sample.exe"

[Setup]

; ���������� ������������� ����������, 
;��������������� ����� Tools -> Generate GUID
AppId={{DE918535-6E80-4DA3-9B79-AA42F17A8F42}}

; ������ ����������, ������������ ��� ���������
AppName={#Name}
AppVersion={#Version}
AppPublisher={#Publisher}

; ���� ��������� ��-���������
DefaultDirName={pf}\{#Name}
; ��� ������ � ���� "����"
DefaultGroupName={#Name}

; �������, ���� ����� ������� ��������� setup � ��� ������������ �����
OutputDir=E:\work\test-setup
OutputBaseFileName=TestInstall

; ���� ������
SetupIconFile=E:\work\icon.ico

; ��������� ������
Compression=lzma
SolidCompression=yes


[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"; LicenseFile: "License.txt"
Name: "russian"; MessagesFile: "compiler:Languages\Russian.isl"; 

[Tasks]
; �������� ������ �� ������� �����
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked


[Files]

; ����������� ����
Source: "E:\work\lab_9\Debug\sfml_sample.exe"; DestDir: "{app}"; Flags: ignoreversion

; ������������� �������
Source: "E:\work\lab_9\*"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs createallsubdirs

; VS Redistributable package
Source: "E:\work\VC_redist.x64.exe"; DestDir: "{tmp}"; Flags: deleteafterinstall


[Icons]

Name: "{group}\{#Name}"; Filename: "{app}\{#ExeName}"

Name: "{commondesktop}\{#Name}"; Filename: "{app}\{#ExeName}"; Tasks: desktopicon
Name: "{autodesktop}\{#Name}"; IconFilename: "E:\work\icon.ico"; Filename: "{app}\{#ExeName}"; Tasks: desktopicon
[Run]

Filename: {tmp}\VC_redist.x64.exe; Parameters: "/q:a /c:""install /l /q"""; StatusMsg: VS redistributable package is installed. Please wait...
